<?php
namespace app\index\controller;
use \think\Controller;
use \think\Db;

class Article extends Controller{

    //前台文章内容
    public function index(){
        $id = input("id");
        $article = Db::table("article")->find($id);
        $this->assign("article",$article);
        return $this->fetch("article");
    }
}