<?php
    namespace app\index\controller;
    use think\Controller;
    use think\Db;
    use app\index\model\Article as ArticleModel;

    class Index extends Controller{

        //首页列出文章列表
        public function index(){
            $list = ArticleModel::select();
            if (empty($list)){
                $list = ArticleModel::paginate(3);
            }
            else{
                $list = ArticleModel::order("id desc")->paginate(3);
            }
            $this->assign("list",$list);
            return $this->fetch("index");
        }

    }