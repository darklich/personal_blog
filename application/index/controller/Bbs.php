<?php
namespace app\index\controller;
use \think\Controller;
use \app\index\model\Bbs as BbsModel;
use think\Db;
use think\captcha\Captcha;

class Bbs extends Controller{

    //前台列出留言和添加留言
    public function index(){
        if (request()->isPost()){
            $captcha = new Captcha();
            if (!$captcha->check(input("code"))){
                $this->error("验证码错误");
            }
            $data = [
                "name" => input("name"),
                "message" => input("message"),
                "uptime" => time(),
            ];
            if($data["name"]==""||$data["message"]==""){
                $this->error("就算绕过html也不能是空留言");
            }
            if (Db::table("bbs")->insert($data)){
                $this->success("留言成功");
            }
            else{
                $this->error("留言失败");
            }
        }

        $list = BbsModel::select();
        if (empty($list)){
            $list = BbsModel::paginate(3);
            $this->assign("x",0);
        }
        else{
            $list = BbsModel::order("id desc")->paginate(1);
            $this->assign("x",BbsModel::order("id desc")->count());
        }
        $this->assign("list",$list);
        return $this->fetch("bbs");
    }


}