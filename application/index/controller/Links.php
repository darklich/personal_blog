<?php
    namespace app\index\controller;
    use \think\Controller;
    use app\index\model\Links as LinksModel;

    class Links extends Controller{

        //前台友情链接
        public function index(){
            $list = LinksModel::select();
            if (empty($list)){
                $list = LinksModel::paginate(3);
            }
            else{
                $list = LinksModel::order("id desc")->paginate(3);
            }
            $this->assign("list",$list);
            return $this->fetch("links");
        }
    }