<?php
    namespace app\admin\model;
    use think\captcha\Captcha;
    use think\Db;
    use think\Model;

    class Admin extends Model{
        public function login($data){
            $captcha = new Captcha();
            if (!$captcha->check($data["code"])){
                return 4;  //验证码错误
            }

            $admin = Db::table("admin")->where("name","=",$data["name"])->find();

            if ($admin){
                if ($admin["pwd"]==md5($data["pwd"])){
                    session("name",$admin["name"]);
                    session("id",$admin["id"]);
                    return 1;//登陆成功
                }
                else{
                    return 2;//密码错误
                }
            }
            else{
                return 3;//用户不存在
            }
        }
    }