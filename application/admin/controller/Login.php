<?php
    namespace app\admin\controller;
    use think\Controller;
    use app\admin\model\Admin;
    class Login extends Controller {

        //登录过程
        public function index(){
            if (request()->isPost()){
                $data = [
                    "name" => input("name"),
                    "pwd" => input("pwd"),
                    "code" => input("code"),
                ];
                $admin = new Admin();
                switch ($admin->login($data)){
                    case 1:
                        $this->success("信息正确","index/index");
                        break;
                    case 2:
                        $this->error("密码错误");
                        break;
                    case 3:
                        $this->error("用户不存在");
                        break;
                    case 4:
                        $this->error("验证码错误");
                        break;
                }
            }
            return $this->fetch("login");
        }
    }