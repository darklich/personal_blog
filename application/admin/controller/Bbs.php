<?php
namespace app\admin\controller;
use think\Controller;
use think\Db;
use app\admin\model\Bbs as BbsModel;

class Bbs extends Base {

    //列出留言
    public function lst(){
        $list = BbsModel::select();

        if (empty($list)){
            $list = BbsModel::paginate(3);
        }
        else{
            $list = BbsModel::order("id desc")->paginate(3);
        }
        $this->assign("list",$list);
        return $this->fetch();
    }

    //删除留言
    public function del(){
        $id = input('id');
        if (Db::table("bbs")->delete($id)){
            $this->success("删除留言成功","lst");
        }
        else{
            $this->error("删除留言失败","lst");
        }
    }
}