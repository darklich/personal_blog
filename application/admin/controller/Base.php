<?php
namespace app\admin\controller;
use think\Controller;

class Base extends Controller{

    //初始化方法
    public function _initialize()
    {
        if (!session("name")){
            $this->error("请先登录管理员","login/index");
        }
    }
}