<?php
namespace app\admin\controller;
use think\Controller;
use think\Db;
use app\admin\model\Links as LinksModel;

class Links extends Base {

    //列出连接
    public function lst(){
        $list = LinksModel::paginate(3);
        $this->assign("list",$list);
        return $this->fetch();
    }

    //添加连接
    public function add(){
        if (request()->isPost()){
            $data = [
                "title" => input("title"),
                "url" => (input("url"))
            ];
            if (Db::name("links")->insert($data)){
                return $this->success("添加成功","lst");
            }
            else{
                return $this->error("添加失败");
            }
        }
        return $this->fetch();
    }

    //修改连接
    public function edit(){
        if (request()->isPost()) {
            $data = [
                "id" => input("id"),
                "title" => input("title"),
                "url" => md5(input("url"))
            ];
            if (Db::table("links")->update($data)) {
                $this->success("修改成功", "lst");
            } else {
                $this->error("修改失败", "lst");
            }
        }
        $id = input('id');
        $link = Db::table("links")->find($id);
        $this->assign("link",$link);
        return $this->fetch();
    }

    //删除链接
    public function del(){
        $id = input('id');
        if (Db::table("links")->delete($id)){
            $this->success("删除链接成功","lst");
        }
        else{
            $this->error("删除链接失败","lst");
        }
    }
}