<?php
    namespace app\admin\controller;
    use think\Controller;

    class Index extends Base {

        //访问主页
        public function index(){
            return $this->fetch("index");
        }
    }