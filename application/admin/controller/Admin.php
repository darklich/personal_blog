<?php
namespace app\admin\controller;
use think\Controller;
use think\Db;
use app\admin\model\Admin as AdminModel;

class Admin extends Base {

    //得到管理员列表
    public function lst(){
        $list = AdminModel::paginate(3);
        $this->assign("list",$list);
        return $this->fetch();
    }

    //添加管理员
    public function add(){
        if (request()->isPost()){
            $data = [
                "name" => input("name"),
                "pwd" => md5(input("pwd"))
            ];
            if (Db::name("admin")->insert($data)){
                return $this->success("添加成功","lst");
            }
            else{
                return $this->error("添加失败");
            }
        }
        return $this->fetch();
    }

    //修改管理员信息
    public function edit(){
        if (request()->isPost()){
            $data = [
                "id" => input("id"),
                "name" => input("name"),
                "pwd" => md5(input("pwd"))
            ];
            if (Db::table("admin")->update($data)){
                $this->success("修改成功","lst");
            }
            else{
                $this->error("修改失败","lst");
            }

        }
        $id = input('id');
        if ($id==1){
            $this->error("初始化管理员信息不能修改","lst");
        }
        $admins = Db::table("admin")->find($id);
        $this->assign("admins",$admins);
        return $this->fetch();
    }

    //删除管理员
    public function del(){
        $id = input('id');
        if ($id != 1){
            if (Db::table("admin")->delete($id)){
                $this->success("删除管理员成功","lst");
            }
            else{
                $this->error("删除管理员失败","lst");
            }
        }
        else{
            $this->error("初始化管理员不能删除","lst");
        }
    }

    //注销登录
    public function logout(){
        session(null);

        $this->success("退出成功","Login/index");
    }
}