<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:80:"G:\phpstudy\PHPTutorial\WWW\thinkphp/application/index\view\article\article.html";i:1545320664;s:76:"G:\phpstudy\PHPTutorial\WWW\thinkphp\application\index\view\common\head.html";i:1545303497;s:76:"G:\phpstudy\PHPTutorial\WWW\thinkphp\application\index\view\common\foot.html";i:1545308128;}*/ ?>
<!doctype html>
<html lang="zh">

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<title>巫妖个人博客</title>
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<link rel="stylesheet" type="text/css" href="/public/static/index/css/index.css" media="all" />
		<link rel="stylesheet" type="text/css" href="/public/static/index/css/detail.css" />
	</head>

	<body class="home blog custom-background round-avatars" itemscope itemtype="http://schema.org/Organization">
	
<div class="Yarn_Background" style="background-image: url(/public/static/index/images/47fb3c_.jpg);"></div>

<div class="navi aos-init aos-animate open" data-aos="fade-down">
    <div class="bt-nav">
        <div class="line line1"></div>
        <div class="line line2"></div>
        <div class="line line3"></div>
    </div>
    <div class="navbar animated fadeInRight">
        <div class="inner">
            <nav id="site-navigation" class="main-navigation">
                <div id="main-menu" class="main-menu-container">
                    <div class="menu-menu-container">
                        <ul id="primary-menu" class="menu">
                            <li id="menu-item-17" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-17">
                                <a href="<?php echo url('index/index'); ?>">首页</a>
                            </li>
                            <li id="menu-item-78" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-78">
                                <a href="<?php echo url('links/index'); ?>">链接</a>
                            </li>
                            <li id="menu-item-57" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-57">
                                <a href="<?php echo url('bbs/index'); ?>">留言</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <!-- #site-navigation -->
        </div>
    </div>
</div>

<header id="masthead" class="overlay animated from-bottom" itemprop="brand">
    <div class="site-branding text-center">
        <a href="">
            <figure>
                <img class="custom-logo avatar" src="/public/static/index/images/omikron.png" />
            </figure>
        </a>
        <h3 class="blog-description"><p>巫妖终究是巫妖</p></h3>
    </div>
    <!-- .site-branding -->
    <div class="decor-part">
        <div id="particles-js"></div>
    </div>
    <div class="animation-header">
        <div class="decor-wrapper">
            <svg id="header-decor" class="decor bottom" xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 100 100" preserveAspectRatio="none">
                <path class="large left" d="M0 0 L50 50 L0 100" fill="rgba(255,255,255, .1)"></path>
                <path class="large right" d="M100 0 L50 50 L100 100" fill="rgba(255,255,255, .1)"></path>
                <path class="medium left" d="M0 100 L50 50 L0 33.3" fill="rgba(255,255,255, .3)"></path>
                <path class="medium right" d="M100 100 L50 50 L100 33.3" fill="rgba(255,255,255, .3)"></path>
                <path class="small left" d="M0 100 L50 50 L0 66.6" fill="rgba(255,255,255, .5)"></path>
                <path class="small right" d="M100 100 L50 50 L100 66.6" fill="rgba(255,255,255, .5)"></path>
                <path d="M0 99.9 L50 49.9 L100 99.9 L0 99.9" fill="rgba(255,255,255, 1)"></path>
                <path d="M48 52 L50 49 L52 52 L48 52" fill="rgba(255,255,255, 1)"></path>
            </svg>
        </div>
    </div>
</header>

		<div id="main" class="content">
			<div class="container">
				<article id="post-1202" class="js-gallery post-1202 post type-post status-publish format-standard has-post-thumbnail hentry category-uncategorized tag-11 tag-22 tag-29">
					<style>
						.container {
							padding: 10px 0px;
						}
						
						.post {
							margin: 0 auto
						}
					</style>
					<section class="post_content">
						<header class="post_header">
							<h1 class="post_title"><?php echo $article['title']; ?></h1>
						</header>
						<div class="post-body js-gallery">
							<?php if($article['image'] != ''): ?>
								<img src="/public/<?php echo $article['image']; ?>" height="50">
							<?php endif; ?>
							<?php echo $article['content']; ?>
						</div>
						<time><?php echo date("Y-m-d",$article['uptime']); ?></time>

					</section>
				</article>
			</div>
			<svg id="upTriangleColor" width="100%" height="40" viewBox="0 0 100 102" preserveAspectRatio="none">
				<path d="M0 0 L50 100 L100 0 Z"></path>
			</svg>

			
			<svg id="dwTriangleColor" width="100%" height="40" viewBox="0 0 100 102" preserveAspectRatio="none">
				<path d="M0 0 L50 100 L100 0 Z"></path>
			</svg>
			<div class="wrapper">
			</div>
		</div>

		<div class="p-header">
			<figure class="p-image" style="background-image: url(statics/images/47fb3c_9afed6c259f94589881bd55376206366mv2_d_3840_5784_s_4_2.jpg);"></figure>
		</div>


	<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<footer id="footer" class="overlay animated from-top">
    <div class="decor-wrapper">
        <svg id="footer-decor" class="decor top" xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 100 100" preserveAspectRatio="none">
            <path class="large left" d="M0 0 L50 50 L0 100" fill="rgba(255,255,255, .1)"></path>
            <path class="large right" d="M100 0 L50 50 L100 100" fill="rgba(255,255,255, .1)"></path>
            <path class="medium left" d="M0 0 L50 50 L0 66.6" fill="rgba(255,255,255, .3)"></path>
            <path class="medium right" d="M100 0 L50 50 L100 66.6" fill="rgba(255,255,255, .3)"></path>
            <path class="small left" d="M0 0 L50 50 L0 33.3" fill="rgba(255,255,255, .5)"></path>
            <path class="small right" d="M100 0 L50 50 L100 33.3" fill="rgba(255,255,255, .5)"></path>
            <path d="M0 0 L50 50 L100 0 L0 0" fill="rgba(255,255,255, 1)"></path>
            <path d="M48 48 L50 51 L52 48 L48 48" fill="rgba(255,255,255, 1)"></path>
        </svg>
    </div>
    <div class="socialize" data-aos="zoom-in">
        <li>
            <a title="weibo" class="socialicon" href=""><i class="iconfont" aria-hidden="true">&#xe601;
            </i></a>
        </li>
        <li class="wechat">
            <a class="socialicon"><i class="iconfont">&#xe609;
            </i></a>
            <div class="wechatimg"><img src="/public/static/index/images/LICH.jpg"></div>
        </li>
        <li>
            <a title="QQ" class="socialicon" href="" target="_blank"><i class="iconfont" aria-hidden="true">&#xe616;
            </i></a>
        </li>
    </div>
    <div class="cr">Copyright&copy; 2018. Design by
        <a href="">17sucai</a>
    </div>

</footer>
</body>
</html>
			<script type='text/javascript' src='/public/static/index/js/jquery.min.js'></script>
			<script type='text/javascript' src='/public/static/index/js/plugins.js'></script>
			<script type='text/javascript' src='/public/static/index/js/script.js'></script>
			<script type='text/javascript' src='/public/static/index/js/particles.js'></script>
			<script type='text/javascript' src='/public/static/index/js/aos.js'></script>
			<script type='text/javascript' src='/public/static/index/js/prism.js'></script>
			<script type='text/javascript' src='/public/static/index/js/gravatar.js'></script>

	</body>

</html>