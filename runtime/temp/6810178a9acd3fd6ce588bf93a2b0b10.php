<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:72:"G:\phpstudy\PHPTutorial\WWW\thinkphp/application/index\view\bbs\bbs.html";i:1545574174;s:76:"G:\phpstudy\PHPTutorial\WWW\thinkphp\application\index\view\common\head.html";i:1545303497;s:76:"G:\phpstudy\PHPTutorial\WWW\thinkphp\application\index\view\common\foot.html";i:1545308128;}*/ ?>
<!doctype html>
<html lang="zh">

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<title>巫妖个人博客</title>
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<link rel="stylesheet" type="text/css" href="/public/static/index/css/index.css" media="all" />
		<style>
			.pagination{display:inline-block;padding-left:0;margin:20px 0;border-radius:4px}
			.pagination>li{display:inline}
			.pagination>li>a,
			.pagination>li>span{position:relative;float:left;padding:6px 12px;margin-left:-1px;line-height:1.42857143;color:#428bca;text-decoration:none;background-color:#fff;border:1px solid #ddd}
			.pagination>li:first-child>a,
			.pagination>li:first-child>span{margin-left:0;border-top-left-radius:4px;border-bottom-left-radius:4px}
			.pagination>li:last-child>a,
			.pagination>li:last-child>span{border-top-right-radius:4px;border-bottom-right-radius:4px}
			.pagination>li>a:hover,
			.pagination>li>span:hover,
			.pagination>li>a:focus,
			.pagination>li>span:focus{color:#2a6496;background-color:#eee;border-color:#ddd}
			.pagination>.active>a,.pagination>.active>span,.pagination>.active>a:hover,
			.pagination>.active>span:hover,.pagination>.active>a:focus,.pagination>.active>span:focus{z-index:2;color:#fff;cursor:default;background-color:#428bca;border-color:#428bca}.pagination>.disabled>span,.pagination>.disabled>span:hover,.pagination>.disabled>span:focus,.pagination>.disabled>a,.pagination>.disabled>a:hover,.pagination>.disabled>a:focus{color:#777;cursor:not-allowed;background-color:#fff;border-color:#ddd}.pagination-lg>li>a,.pagination-lg>li>span{padding:10px 16px;font-size:18px}.pagination-lg>li:first-child>a,.pagination-lg>li:first-child>span{border-top-left-radius:6px;border-bottom-left-radius:6px}.pagination-lg>li:last-child>a,.pagination-lg>li:last-child>span{border-top-right-radius:6px;border-bottom-right-radius:6px}.pagination-sm>li>a,.pagination-sm>li>span{padding:5px 10px;font-size:12px}.pagination-sm>li:first-child>a,.pagination-sm>li:first-child>span{border-top-left-radius:3px;border-bottom-left-radius:3px}.pagination-sm>li:last-child>a,.pagination-sm>li:last-child>span{border-top-right-radius:3px;border-bottom-right-radius:3px}
		</style>
	</head>

	<body class="home blog custom-background round-avatars">
	
<div class="Yarn_Background" style="background-image: url(/public/static/index/images/47fb3c_.jpg);"></div>

<div class="navi aos-init aos-animate open" data-aos="fade-down">
    <div class="bt-nav">
        <div class="line line1"></div>
        <div class="line line2"></div>
        <div class="line line3"></div>
    </div>
    <div class="navbar animated fadeInRight">
        <div class="inner">
            <nav id="site-navigation" class="main-navigation">
                <div id="main-menu" class="main-menu-container">
                    <div class="menu-menu-container">
                        <ul id="primary-menu" class="menu">
                            <li id="menu-item-17" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-17">
                                <a href="<?php echo url('index/index'); ?>">首页</a>
                            </li>
                            <li id="menu-item-78" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-78">
                                <a href="<?php echo url('links/index'); ?>">链接</a>
                            </li>
                            <li id="menu-item-57" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-57">
                                <a href="<?php echo url('bbs/index'); ?>">留言</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <!-- #site-navigation -->
        </div>
    </div>
</div>

<header id="masthead" class="overlay animated from-bottom" itemprop="brand">
    <div class="site-branding text-center">
        <a href="">
            <figure>
                <img class="custom-logo avatar" src="/public/static/index/images/omikron.png" />
            </figure>
        </a>
        <h3 class="blog-description"><p>巫妖终究是巫妖</p></h3>
    </div>
    <!-- .site-branding -->
    <div class="decor-part">
        <div id="particles-js"></div>
    </div>
    <div class="animation-header">
        <div class="decor-wrapper">
            <svg id="header-decor" class="decor bottom" xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 100 100" preserveAspectRatio="none">
                <path class="large left" d="M0 0 L50 50 L0 100" fill="rgba(255,255,255, .1)"></path>
                <path class="large right" d="M100 0 L50 50 L100 100" fill="rgba(255,255,255, .1)"></path>
                <path class="medium left" d="M0 100 L50 50 L0 33.3" fill="rgba(255,255,255, .3)"></path>
                <path class="medium right" d="M100 100 L50 50 L100 33.3" fill="rgba(255,255,255, .3)"></path>
                <path class="small left" d="M0 100 L50 50 L0 66.6" fill="rgba(255,255,255, .5)"></path>
                <path class="small right" d="M100 100 L50 50 L100 66.6" fill="rgba(255,255,255, .5)"></path>
                <path d="M0 99.9 L50 49.9 L100 99.9 L0 99.9" fill="rgba(255,255,255, 1)"></path>
                <path d="M48 52 L50 49 L52 52 L48 52" fill="rgba(255,255,255, 1)"></path>
            </svg>
        </div>
    </div>
</header>

		<div id="main" class="content">
			<div class="container">
				<article id="post-2" class="js-gallery post-2 page type-page status-publish has-post-thumbnail hentry">
					<style>
						.meta {
							display: none;
						}
						
						#NextPrevPosts {
							margin: 0;
							visibility: hidden;
						}
					</style>
					<section class="post_content">
						<header class="post_header">
							<h1 class="post_title">访客留言</h1>
						</header>

					</section>
				</article>
			</div>
			<svg id="upTriangleColor" width="100%" height="40" viewBox="0 0 100 102" preserveAspectRatio="none">
				<path d="M0 0 L50 100 L100 0 Z"></path>
			</svg>
			<div id="panel">
				<div class="comment-area">
					<section class="comments">
						<div class="comments-main">
							<div id="comments-list-title"><span><?php echo $x; ?></span> 条评论 </div>
							<div id="loading-comments">
								<div class="host">
									<div class="loading loading-0"></div>
									<div class="loading loading-1"></div>
									<div class="loading loading-2"></div>
								</div>
							</div>
							<ul class="commentwrap">
								<?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
								<li class="comment even thread-even depth-1" id="li-comment-">
									<div id="comment-936" class="comment_body contents">
										<div class="main shadow">
											<div class="commentinfo">
												<section class="commeta">
													<div class="shang">
														<h4 class="author"><?php echo $vo['name']; ?></h4>
													</div>
												</section>
											</div>
											<div class="body">
												<p><?php echo $vo['message']; ?></p>
											</div>
											<div class="xia info">
												<span><time><?php echo date("Y-m-d",$vo['uptime']); ?></time></span>
											</div>
										</div>
									</div>
								</li>
								<?php endforeach; endif; else: echo "" ;endif; ?>
							</ul>
							<div style="text-align: center;margin-top: 10px"><?php echo $list->render(); ?></div>

							<div id="respond" class="comment-respond">
								<form action="" method="post" id="commentform" class="clearfix">
									<div class="author-info">
										<input type="text" name="name" id="name" placeholder="昵  称 : " value="" tabindex="1" required="" title="Name (required)" />
										<input type="text" name="code" id="code" placeholder="验证码 : " value="" tabindex="1" required="" title="Name (required)" />
										<img src="<?php echo captcha_src(); ?>" alt="captcha" style="float: right" onclick="this.src='<?php echo captcha_src(); ?>?' + Math.random()"/>
									</div>

									<div class="comment-form-info">
										<div class="real-time-gravatar">
											&nbsp&nbsp&nbsp&nbsp&nbsp
										</div>
										<p class="input-row">
											<textarea class="text_area" rows="3" cols="60" name="message" id="message" tabindex="4" placeholder="你不说两句吗？(°∀°)ﾉ……" required></textarea>
											<input type="submit" name="submit" class="button" id="submit" tabindex="5" value="发送" />
										</p>
									</div>
								</form>
							</div>
						</div>
					</section>
				</div>
			</div>
			<svg id="dwTriangleColor" width="100%" height="40" viewBox="0 0 100 102" preserveAspectRatio="none">
				<path d="M0 0 L50 100 L100 0 Z"></path>
			</svg>
			<div class="wrapper">
			</div>
		</div>

	<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<footer id="footer" class="overlay animated from-top">
    <div class="decor-wrapper">
        <svg id="footer-decor" class="decor top" xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 100 100" preserveAspectRatio="none">
            <path class="large left" d="M0 0 L50 50 L0 100" fill="rgba(255,255,255, .1)"></path>
            <path class="large right" d="M100 0 L50 50 L100 100" fill="rgba(255,255,255, .1)"></path>
            <path class="medium left" d="M0 0 L50 50 L0 66.6" fill="rgba(255,255,255, .3)"></path>
            <path class="medium right" d="M100 0 L50 50 L100 66.6" fill="rgba(255,255,255, .3)"></path>
            <path class="small left" d="M0 0 L50 50 L0 33.3" fill="rgba(255,255,255, .5)"></path>
            <path class="small right" d="M100 0 L50 50 L100 33.3" fill="rgba(255,255,255, .5)"></path>
            <path d="M0 0 L50 50 L100 0 L0 0" fill="rgba(255,255,255, 1)"></path>
            <path d="M48 48 L50 51 L52 48 L48 48" fill="rgba(255,255,255, 1)"></path>
        </svg>
    </div>
    <div class="socialize" data-aos="zoom-in">
        <li>
            <a title="weibo" class="socialicon" href=""><i class="iconfont" aria-hidden="true">&#xe601;
            </i></a>
        </li>
        <li class="wechat">
            <a class="socialicon"><i class="iconfont">&#xe609;
            </i></a>
            <div class="wechatimg"><img src="/public/static/index/images/LICH.jpg"></div>
        </li>
        <li>
            <a title="QQ" class="socialicon" href="" target="_blank"><i class="iconfont" aria-hidden="true">&#xe616;
            </i></a>
        </li>
    </div>
    <div class="cr">Copyright&copy; 2018. Design by
        <a href="">17sucai</a>
    </div>

</footer>
</body>
</html>
			<script type='text/javascript' src='/public/static/index/js/jquery.min.js'></script>
			<script type='text/javascript' src='/public/static/index/js/plugins.js'></script>
			<script type='text/javascript' src='/public/static/index/js/script.js'></script>
			<script type='text/javascript' src='/public/static/index/js/particles.js'></script>
			<script type='text/javascript' src='/public/static/index/js/aos.js'></script>

	</body>

</html>